import csv
from io import StringIO
from pprint import pprint

import requests
from celery import shared_task
from datapackage import Package

from django_mdat_location.django_mdat_location.models import MdatCities, MdatCountries
from django_mdat_phone.django_mdat_phone.models import (
    MdatPhoneNationalCode,
    MdatPhoneCountryCode,
)


@shared_task(name="mdat_phone_sync_from_opengeodb")
def mdat_phone_sync_from_opengeodb():
    opengeodb_csv_file_request = requests.get(
        "http://www.fa-technik.adfc.de/code/opengeodb/DE.tab"
    )

    opengeodb_csv_file = StringIO(opengeodb_csv_file_request.text)

    reader = csv.reader(opengeodb_csv_file, delimiter="\t")

    # skip the headers
    next(reader, None)

    for row in reader:
        if not row[7].replace(" ", "") == "" and not row[8].replace(" ", "") == "":
            for zip_code in row[7].split(","):
                for tel_national_code in row[8].replace(" ", "").split(","):
                    zip_code = zip_code.strip()
                    tel_national_code = int(tel_national_code.strip())

                    print("PLZ " + zip_code.strip() + ": " + str(tel_national_code))

                    try:
                        city = MdatCities.objects.get(
                            country=MdatCountries.objects.get(iso="DE"),
                            zip_code=zip_code.strip(),
                        )
                    except (
                        MdatCities.DoesNotExist,
                        MdatCities.MultipleObjectsReturned,
                    ):
                        continue

                    try:
                        mdat_national_code = MdatPhoneNationalCode.objects.get(
                            city=city,
                            national_code=tel_national_code,
                        )
                    except MdatPhoneNationalCode.DoesNotExist:
                        tel_national_code = MdatPhoneNationalCode(
                            city=city,
                            national_code=tel_national_code,
                        )
                        tel_national_code.save()

    return


@shared_task(name="mdat_phone_sync_from_datahub_country_codes")
def mdat_phone_sync_from_datahub_country_codes():
    package = Package("https://datahub.io/core/country-codes/datapackage.json")

    # print processed tabular data (if exists any)
    for resource in package.resources:
        if resource.descriptor["datahub"]["type"] == "derived/csv":
            data = resource.read(keyed=True)

            for row in data:
                # Check if ISO code is present
                if "ISO3166-1-numeric" not in row or "Dial" not in row:
                    continue

                if row["ISO3166-1-numeric"] == "" or row["ISO3166-1-numeric"] is None:
                    continue

                if row["Dial"] == "" or row["Dial"] is None:
                    continue

                row_country_code = int(row["ISO3166-1-numeric"])

                # Prepare Country Code
                dial_code = row["Dial"].replace("-", "")

                if dial_code.isdigit():
                    dial_code = int(dial_code)
                else:
                    continue

                # Process information
                try:
                    country = MdatCountries.objects.get(num_code=row_country_code)
                except MdatCountries.DoesNotExist:
                    pprint("UNKNOWN COUNTRY CODE: " + str(row_country_code))
                    pprint(row)
                    continue

                try:
                    country_code = MdatPhoneCountryCode.objects.get(
                        country=country,
                        country_code=dial_code,
                    )
                except MdatPhoneCountryCode.DoesNotExist:
                    country_code = MdatPhoneCountryCode(
                        country=country,
                        country_code=dial_code,
                    )
                else:
                    continue

                country_code.save()

    return
