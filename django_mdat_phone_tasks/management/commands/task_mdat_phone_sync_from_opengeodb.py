from django.core.management.base import BaseCommand

from django_mdat_phone_tasks.django_mdat_phone_tasks.tasks import (
    mdat_phone_sync_from_opengeodb,
)


class Command(BaseCommand):
    help = "Run task mdat_phone_sync_from_opengeodb"

    def handle(self, *args, **options):
        self.stdout.write(self.help)

        mdat_phone_sync_from_opengeodb()

        self.stdout.write(self.style.SUCCESS("Successfully finished."))
